using GoogleARCore;
using System;
using UnityEngine;

/// <summary>
/// Renders an AR visualization when an AR Marker is tracked.
/// </summary>
public class ARRenderer : MonoBehaviour
{
    /// <summary>
    /// Tracked AR Marker
    /// </summary>
    public AugmentedImage Image;

    /// <summary>
    /// A model for the cruise ship.
    /// </summary>
    public GameObject CruiseShip;

    /// <summary>
    /// Rotation speed of the rendered model when toggled.
    /// </summary>
    public float RotationSpeed = 10f;

    private bool _shouldRotate = false;

    public void Update()
    {
        if (Image == null || Image.TrackingState != TrackingState.Tracking)
        {
            CruiseShip.SetActive(false);
            return;
        }

        //Update the 3D models position that is being rendered to the center position of the AR Marker.
        CruiseShip.transform.localPosition = Image.CenterPose.position;

        if (_shouldRotate)
        {
            transform.Rotate(Vector3.up * RotationSpeed * Time.deltaTime);
        }

        CruiseShip.SetActive(true);
    }

    public void ToggleRotation()
    {
        _shouldRotate = !_shouldRotate;
    }
}
