using System.Collections.Generic;
using GoogleARCore;
using UnityEngine;

/// <summary>
/// Scene controller for AR Marker tracking.
/// </summary>
public class ARMarkerController : MonoBehaviour
{
    /// <summary>
    /// A prefab for visualizing an ARMarker.
    /// </summary>
    public ARRenderer ARPrefab;

    private Dictionary<int, ARRenderer> _renders = new Dictionary<int, ARRenderer>();
    private List<AugmentedImage> _augmentedImages = new List<AugmentedImage>();

    /// <summary>
    /// The Unity Update method.
    /// </summary>
    public void Update()
    {
        // Exit the app when the 'back' button is pressed.
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (Input.GetKey(KeyCode.Mouse0))
        {
            foreach (var render in _renders.Values)
            {
                render.ToggleRotation();
            }
        }

        // Check that motion tracking is tracking.
        if (Session.Status != SessionStatus.Tracking)
        {
            return;
        }

        // Get the updated augmented images for this frame.
        Session.GetTrackables<AugmentedImage>(_augmentedImages, TrackableQueryFilter.Updated);
        
        foreach (var image in _augmentedImages)
        {
            ARRenderer render = null;
            _renders.TryGetValue(image.DatabaseIndex, out render);
            // Create visualizers and anchors for tracked AR markers
            if (image.TrackingState == TrackingState.Tracking && render == null)
            {
                // Create an anchor to ensure that ARCore keeps tracking this marker.
                Anchor anchor = image.CreateAnchor(image.CenterPose);
                render = (ARRenderer)Instantiate(ARPrefab, anchor.transform);
                render.Image = image;
                _renders.Add(image.DatabaseIndex, render);
            }
            //Remove visualizers for images no longer being tracked.
            else if (image.TrackingState == TrackingState.Stopped && render != null)
            {
                _renders.Remove(image.DatabaseIndex);
                GameObject.Destroy(render.gameObject);
            }
        }
    }
}
